import React, { Component } from "react";
import { NavItem, Nav, NavDropdown, MenuItem } from "react-bootstrap";
// import {Redirect} from 'react-router-dom';
// import {BrowserRouter} from 'react-router-dom';
class HeaderLinks extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isAuthenticated: false  
    }
    this.logout = this.logout.bind(this);
    
  }
  logout() {
   
    // if(this.state.username && this.state.password){
      // var result="null";
      
      // fetch('http://13.127.242.219/rest-auth/logout/', {
      //   method: 'POST',
      //   headers:{'Content-Type':'application/json',
      //     'X-CSRFToken':'u0nlJbvPzJSrHL5FWeTeCJtF0tPFGADM'
      //           }
       
      //  })
      //  .then(response =>{
         
      //   console.log(response.status)
      //   // console.log(response.statusText)
      //   // console.log(response)
      //   if (response.status===200) {
      //     this.setState({isAuthenticated: true});
      //     // console.log("redirect="+this.state.isAuthenticated);
      //   } else {
      //     console.log("login fail");
      //     // this.handleClick.bind(this,'bl')
      //   }
        
      //  }) 
      // return (<Redirect to={'/login'}/>)
     
    }
  
    
  render() {
    const notification = (
      <div>
        <i className="fa fa-globe" />
        <b className="caret" />
        <span className="notification">5</span>
        <p className="hidden-lg hidden-md">Notification</p>
      </div>
    );

    // if (this.state.isAuthenticated) {
    //   return (<Redirect to={'/Login'}/>)
    //     }


    return (
      <div>
        <Nav>
          <NavItem eventKey={1} href="#">
            <i className="fa fa-dashboard" />
            <p className="hidden-lg hidden-md">Dashboard</p>
          </NavItem>
          <NavDropdown
            eventKey={2}
            title={notification}
            noCaret
            id="basic-nav-dropdown"
          >
            <MenuItem eventKey={2.1}>Notification 1</MenuItem>
            <MenuItem eventKey={2.2}>Notification 2</MenuItem>
            <MenuItem eventKey={2.3}>Notification 3</MenuItem>
            <MenuItem eventKey={2.4}>Notification 4</MenuItem>
            <MenuItem eventKey={2.5}>Another notifications</MenuItem>
          </NavDropdown>
          <NavItem eventKey={3} href="#">
            <i className="fa fa-search" />
            <p className="hidden-lg hidden-md">Search</p>
          </NavItem>
          <NavItem>
            <span>CAMPAIGN-ADMIN</span>
          </NavItem>
        </Nav>
        <Nav pullRight>
          <NavItem eventKey={1} href="#">
            Account
          </NavItem>
          <NavDropdown
            eventKey={2}
            title="Dropdown"
            id="basic-nav-dropdown-right"
          >
            <MenuItem eventKey={2.1}>Action</MenuItem>
            <MenuItem eventKey={2.2}>Another action</MenuItem>
            <MenuItem eventKey={2.3}>Something</MenuItem>
            <MenuItem eventKey={2.4}>Another action</MenuItem>
            <MenuItem eventKey={2.5}>Something</MenuItem>
            <MenuItem divider />
            <MenuItem eventKey={2.5}>Separated link</MenuItem>
          </NavDropdown>
          <NavItem eventKey={3} href="#">
            Log out
          </NavItem>
        </Nav>
      </div>
    );
  }
}

export default HeaderLinks;
