import React, { Component } from "react";
import {
  Grid,
  Row,
  Col,
  FormGroup,
  ControlLabel,
  FormControl
} from "react-bootstrap";
import {connect} from 'react-redux';

import { Card } from "../../components/Card/Card.jsx";
// import { FormInputs } from "../../components/FormInputs/FormInputs.jsx";
import Button from "../../components/CustomButton/CustomButton.jsx";
import * as Alluser from '../../../redux/actions/userDataAction'

import { bindActionCreators } from 'redux';
class UserProfile extends Component {
  constructor(props) {
    super(props);

    this.state = {
      first_name:"",
      last_name:"",
      mobile:"",    
      email: "",
      password1: "",
      password2:'',
      username:'',
      location:'6',      
      token: ''

    };
    // this.usergetById = this.usergetById.bind(this);
    this.userUpdate = this.userUpdate.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.validateForm = this.validateForm.bind(this);
    

  }


 userUpdate() {
   console.log("heloo")
  if((this.state.password1 && this.state.password2)&&this.state.email){
    console.log("heloo")
    fetch('http://13.127.242.219/api/v1/users/21/', {
      method: 'Put',
      headers:{'Content-Type': 'application/json',
                'Accept': 'application/json',
               'X-CSRFToken':'Ml5931cJA7dWvYxgRVKSpy63E1Gd2fd6'
              },
      body: JSON.stringify(this.state)
     })
     .then(response =>{
       
      // console.log(response.status)
      // console.log(response.statusText)
      // console.log(response)
      if (response.status===202) {
        // this.setState({isAuthenticated: true});
        console.log("update ");
      } else {
        console.log("login fail");
        // this.handleClick.bind(this,'bl')
      }
      response.json().then(body => {  
        // console.log(body)
        this.setState({token:body});
        // console.log(this.state.token)
       })

     })
    
   
   }
   
  }
  validateForm() {   

  }

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  }

  handleSubmit = event => {
    event.preventDefault();
    console.log("hi")
    this.userUpdate();
    // this.props.actions.updatePassword(this.state);
  }
  render() {
    const userData=this.props.userData;
    return (
      <div className="content">
        <Grid fluid>
          <Row>
            <Col md={2}></Col>
            <Col md={8}>
              <Card
                title="Edit Profile"
                content={
                  <form onSubmit={this.handleSubmit}>
                     <Row>
                       <Col md={6}>
                        <FormGroup controlId="username" >
                          <ControlLabel>UserName</ControlLabel>
                          <FormControl
                            bsClass="form-control"
                            placeholder="Username"
                            autoFocus
                            type="text"
                            disabled
                            value={userData.username||""}
                            onChange={this.handleChange}
                          />
                        </FormGroup>                      
                       </Col>
                       <Col md={6}>
                       <FormGroup controlId="mobile" >
                          <ControlLabel>Mobile Number</ControlLabel>
                         
                          <FormControl
                            bsClass="form-control"
                            placeholder="Mobile Number"
                            autoFocus
                            disabled
                            type="text"
                            value={this.props.allUserData.mobile||""}
                            onChange={this.handleChange}
                          />
                        </FormGroup>
                       </Col>
                      </Row>
                      <Row>
                       <Col md={6}>
                        <FormGroup controlId="firstname" >
                          <ControlLabel>First Name </ControlLabel>
                          <FormControl
                            bsClass="form-control"
                            placeholder="Pankaj "
                            autoFocus
                            type="text"
                            disabled
                            value={userData.first_name||""}
                            onChange={this.handleChange}
                          />
                        </FormGroup>                      
                       </Col>
                       <Col md={6}>
                       <FormGroup controlId="lastname" >
                          <ControlLabel>Last Name</ControlLabel>
                          <FormControl
                            bsClass="form-control"
                            placeholder="Kumar "
                            autoFocus
                            disabled
                            type="text"
                            value={userData.last_name||""}
                            onChange={this.handleChange}
                          />
                        </FormGroup>
                       </Col>
                      </Row>
                      <Row>
                        <Col md={12}>
                        <FormGroup controlId="email" >
                          <ControlLabel>email</ControlLabel>
                          <FormControl
                            bsClass="form-control"
                            placeholder="Pankaj@xyz"
                            autoFocus
                            
                            type="email"
                            value={userData.email||""}
                            onChange={this.handleChange}
                          />
                        </FormGroup>
                               
                        </Col>
                      </Row>
                      <Row>
                       <Col md={6}>
                        <FormGroup controlId="password1" >
                          <ControlLabel>Old Password</ControlLabel>
                          <FormControl
                            bsClass="form-control"
                            placeholder="********"
                            autoFocus
                            type="password"
                           
                            value={this.state.password1||""}
                            onChange={this.handleChange}
                          />
                        </FormGroup>                      
                       </Col>
                       <Col md={6}>
                       <FormGroup controlId="password2" >
                          <ControlLabel>New Password</ControlLabel>
                          <FormControl
                            bsClass="form-control"
                            placeholder="*******"
                            autoFocus
                            type="password"
                            value={this.state.password2||""}
                            onChange={this.handleChange}
                          />
                        </FormGroup>
                       </Col>
                      </Row>      
                      <Row>
                        <Col md={12}>
                        <FormGroup controlId="location" >
                          <ControlLabel>Location</ControlLabel>
                          <FormControl
                            bsClass="form-control"
                            placeholder="delhi"
                            autoFocus
                            disabled
                            type="text"
                            value={this.props.allUserData.location||""}
                            onChange={this.handleChange}
                          />
                        </FormGroup>
                               
                        </Col>
                      </Row>               
                    <Button bsStyle="info" pullRight fill type="submit" >
                      Update Profile
                    </Button>
                    {/* <Button bsStyle="info"  fill  type="button"  onClick={this.props.actions.getData()}>
                      Sync
                    </Button>
                    <div className="clearfix" />*/}
                   <div onClick={this.props.actions.getData()}>{this.props.Response.status}</div> 
                  </form>
                }
              />
            </Col>           
          </Row>
        </Grid>
      </div>
    );
  }
}

// export default UserProfile;
function mapStateToProps(state) {  
  // this function connect to the indux reducer 
  // console.log("hi mapto=========");
  
  // console.log(state.allUserData);
  
  
  return {
      userData: state.userData.userInfo,
      allUserData:state.userData.allUserData,
      Response:state.userData.Response
  };
}
function matchDispatchToProps(dispatch){   // this function connect to the action ie.creator authAction 
  return{
    actions:bindActionCreators(Alluser, dispatch)   // alluser is the above import authAction  and action is alternative name for access authAction function 
  } 
}

export default connect(mapStateToProps,matchDispatchToProps)(UserProfile);
// alt_mobile: []
// archived: false
// guild: null
// id: 21
// location: null
// mobile: "9929268532"
// number_of_missed_calls: 0
// owner: null
// points: 800
// stage: null
// training_kits_seen: []
// user: {id: 21, email: "0000000010@indusaction.org", first_name: "Test", last_name: "10", profile: 21,…}

// email: "0000000010@indusaction.org"
// first_name: "Test"
// id: 21
// last_name: "10"
// mobile: "9929268532"
// points: 800
// profile: 21
// username: "0000000010"

// user_type: "AD"