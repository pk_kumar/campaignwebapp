import * as types from './actionTypes';
import AllApi from '../../services/api';


function forApiResponse(Response) {
    // console.log("createLoginSuccess inside Response==");
    // console.log(Response);
  
    return {
      type: types.SET_USER_RESPONSE,
      UsergetRequestResponse:Response
    };
  }
  function forApiData(key) {
    // console.log("createLoginSuccessToKey inside key==");
    // console.log(key);
    
    return {
      type: types.SET_USER_BODY,
      userInfo:key
    };
  }

  export function getData() {
    //   console.log("hi i am getData");
      
    
    return dispatch => {
        // console.log("hi i am in dispacher");
        
        AllApi.userGetData().then(Response=>{
            // console.log("hi i am in responce ");
            
  
        dispatch(forApiResponse(Response));
  
        Response.json().then(body=>{
          dispatch(forApiData(body));
        });
        
      });
    }
  }
  
  
// function PasswordUpdateApiResponse(Response) {
//     console.log("PasswordUpdateApiResponse inside Response==");
//     console.log(Response);
  
//     return {
//       type:types.PasswordUpdateApiResponse,
//       responseStatus:Response.status
//     };
//   }
//   function PasswordUpdateApiData(key) {
//     console.log("PasswordUpdateApiData inside key==");
//     console.log(key);
    
//     return {
//       type: types.PasswordUpdateApiData,
//       userToken:key.key
//     };
//   }
  
//   export function updatePassword(userData) {
//     console.log("inside updatePassword befor api call userData =")
//     console.log(userData);
  
//     return dispatch => {  
//       AllApi.userUpdatePassword(userData).then(Response=>{
  
//         console.log("inside userUpdatePassword after api call Response  =")
//         console.log(Response);
       
//         dispatch(PasswordUpdateApiResponse(Response));
  
//         Response.json().then(body=>{
//           dispatch(PasswordUpdateApiData(body));
//         });
  
//       });
//     }
//   }
  