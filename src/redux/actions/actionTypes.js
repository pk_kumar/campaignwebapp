export const SET_LOGIN_PENDING = 'SET_LOGIN_PENDING';
export const SET_USER_RESPONSE = 'SET_USER_RESPONSE';
export const SET_USER_BODY = 'SET_USER_BODY';
export const SET_USER_TOKEN='SET_USER_TOKEN';
export const PasswordUpdateApiData ='PasswordUpdateApiData';
export const PasswordUpdateApiResponse='PasswordUpdateApiResponse';