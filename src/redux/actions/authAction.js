import * as types from './actionTypes';
import AllApi from '../../services/api';

function createLoginSuccess(Response) {
  console.log("createLoginSuccess inside Response==");
  console.log(Response);

  return {
    type: types.SET_LOGIN_PENDING,
    responseStatus:Response.status
  };
}
function createLoginSuccessToKey(key) {
  console.log("createLoginSuccessToKey inside key==");
  console.log(key);
  
  return {
    type: types.SET_USER_TOKEN,
    userToken:key.key
  };
}

export function Login(userData) {
  
  return dispatch => {
      AllApi.login(userData).then(Response=>{

      dispatch(createLoginSuccess(Response));

      Response.json().then(body=>{
        dispatch(createLoginSuccessToKey(body));
      });
      
    });
  }
}
export function Signup(userData) {
  console.log("inside login befor api call =")
  console.log(userData);

  return dispatch => {  
    AllApi.signup(userData).then(Response=>{

      console.log("inside login after api call respons  =")
      console.log(Response.Response);
     
      dispatch(createLoginSuccess(Response));

      Response.json().then(body=>{
        dispatch(createLoginSuccessToKey(body));
      });

    });
  }
}
