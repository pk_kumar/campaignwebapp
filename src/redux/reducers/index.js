
import {combineReducers} from 'redux';
import  AuthUser  from "./AuthUser";
import UserData from "./userData";


const allReducers = combineReducers({
    Auth:AuthUser,
    userData:UserData
});

export default allReducers
