import * as actionTypes from '../actions/actionTypes';
import initialState from './initialState';

export default function(state=initialState.login,action) {
    switch (action.type) {
        case actionTypes.SET_LOGIN_PENDING:
           return  Object.assign({}, state, {
            Status: action.responseStatus,
          }); 
        case actionTypes.SET_USER_TOKEN:
        
            return Object.assign({}, state, {
                Token: action.userToken,
               
              }); 
        default: return state;
    }
      
}
