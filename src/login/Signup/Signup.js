import React, { Component } from "react";
import { Button, FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import "../assets/css/Login.css";
// import PostData from '../services/PostData';
import {Redirect} from 'react-router-dom';
import LoginHeader  from '../Header/Header';
import {
  Grid,
  Row,
  Col, 
} from "react-bootstrap";
import {connect} from 'react-redux';
// import {Redirect} from 'react-router-dom';
import { bindActionCreators } from 'redux';
import * as Alluser from '../../redux/actions/authAction'
class Signup extends Component {
  constructor(props) {
    super(props);

    this.state = {
      first_name:"",
      last_name:"",
      mobile:"",    
      email: "",
      password1: "",
      password2:'',
      username:'',
      location:'6',
      isAuthenticated: false,
      token: ''

    };
    // this.signup = this.signup.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.validateForm = this.validateForm.bind(this);
    

  }
//  signup() {
//   fetch('http://13.127.242.219/rest-auth/registration/', {
//       method: 'POST',
//       headers:{'Content-Type': 'application/json',
//                 'Accept': 'application/json'
//               },
//       body: JSON.stringify(this.state)
//     }).then(response =>{
       
//       console.log(response.status)
//       console.log(response.statusText)
//       console.log(response)
//       if (response.status===201) {
//         this.setState({isAuthenticated: true});
//         console.log("redirect="+this.state.isAuthenticated);
//       } else {
//         console.log("login fail");
//       }
//       response.json().then(body => {  
//         console.log(body)
//         this.setState({token:body});
//         console.log(this.state.token)
//        })

//      })
    
//  }
  validateForm() {   

  }

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  }

  handleSubmit = event => {
    event.preventDefault();
    // this.signup();
    this.props.actions.Signup(this.state); 
  }

  render() {
    if (this.state.isAuthenticated) {
      return (<Redirect to={'/Dashboard'}/>)
        }
    return (
      <div>
        <LoginHeader/>
        <div className="signup " >
          <Grid fluid>
            <Row className="show-grid">
              <Col xs={1} md={1}>     
              </Col>
              <Col xs={10} md={10}>
                <form onSubmit={this.handleSubmit}>
                  <Row>
                    <Col md={12}>
                      <FormGroup controlId="username" >
                        <ControlLabel>User Id</ControlLabel>
                        <FormControl
                          autoFocus
                          type="text"
                          // value={this.state.username}
                          onChange={this.handleChange}
                        />
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col md={6}>
                        <FormGroup controlId="first_name" >
                          <ControlLabel>First Name</ControlLabel>
                          <FormControl
                            autoFocus
                            type="text"
                            // value={this.state.first_name}
                            onChange={this.handleChange}
                          />
                        </FormGroup>
                    </Col>
                    <Col md={6}>
                      <FormGroup controlId="last_name" >
                        <ControlLabel>Last Name</ControlLabel>
                        <FormControl
                          autoFocus
                          type="text"
                          // value={this.state.last_name}
                          onChange={this.handleChange}
                        />
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col md={6}>
                      <FormGroup controlId="email" >
                        <ControlLabel>Email</ControlLabel>
                        <FormControl
                          autoFocus
                          type="email"
                          // value={this.state.email}
                          onChange={this.handleChange}
                        />
                      </FormGroup>
                    </Col>
                    <Col md={6}>
                      <FormGroup controlId="mobile" >
                        <ControlLabel>Mobile No</ControlLabel>
                        <FormControl
                          autoFocus
                          type="text"
                          value={this.state.mobile}
                          onChange={this.handleChange}
                        />
                      </FormGroup>
                    </Col>
                  </Row>
                  <Row>
                    <Col md={6}>
                      <FormGroup controlId="password1" >
                        <ControlLabel>Password</ControlLabel>
                        <FormControl
                          // value={this.state.password1}
                          onChange={this.handleChange}
                          type="password"
                        />
                      </FormGroup>
                    </Col>
                    <Col md={6}>
                      <FormGroup controlId="password2" >
                        <ControlLabel>Confirm Password</ControlLabel>
                        <FormControl
                          // value={this.state.password2}
                          onChange={this.handleChange}
                          type="password"
                        />
                      </FormGroup>
                    </Col>
                  </Row>           
                  <Row>
                    <Col md={12}>
                      <Button block  
                        // disabled={!this.validateForm()}
                        type="submit"
                        // onClick={this.signup}
                      >
                      Signup
                      </Button>
                    </Col>
                </Row>
                
              </form>              
              </Col>
              <Col xs={1} md={1}>            
              </Col>
            </Row>
          </Grid>   
        </div>
      </div>      
    );
  }
}
// export default Signup;

function mapStateToProps(state) {  
  // this function connect to the indux reducer 
  return {
      // users: state.user
  };
}
function matchDispatchToProps(dispatch){   // this function connect to the action ie.creator authAction 
  return{
    actions:bindActionCreators(Alluser, dispatch)   // alluser is the above import authAction  and action is alternative name for access authAction function 
  } 
}
export default connect(mapStateToProps,matchDispatchToProps)(Signup);
