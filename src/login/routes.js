import React, { Component } from "react";
import {BrowserRouter,  Route,  Switch} from 'react-router-dom';
import Login from './loginform/Loginform';
import Signup from './Signup/Signup';
import Admin from "../dashboard/Admin";
import NotFound from './NotFound/NotFound';
import {connect} from 'react-redux';


class AllRoutes extends Component{
    render(){
      return(
        <BrowserRouter >
          <Switch>
              <Route exact path="/" component={Login}/> 
              <Route path="/Login" component={Login}/>         
              <Route path="/Signup" component={Signup}/>
              { (this.props.users===200||this.props.users===201) && <Route path="/Dashboard" component={Admin}/>  
              }                       
              <Route path="*" component={NotFound}/>
          </Switch>
        </BrowserRouter>
      );
    }
  }
function mapStateToProps(state) {  
    // this function connect to the indux reducer 
    return {
        users: state.Auth.Status
    };
  }

export default connect(mapStateToProps)(AllRoutes);
