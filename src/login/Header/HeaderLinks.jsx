import React, { Component } from "react";
import { NavItem, Nav} from "react-bootstrap";
import "../assets/css/Login.css";
import logo from "../assets/img/logo.png";



class HeaderLinks extends Component {
  render() {
    
    return (
      <div> 
        <div>
          {/* <Nav>
          <img src="../assets/img/IA Logo.png" alt="" />
          </Nav> */}
          {/* <Navbar.Header>
            <Navbar.Brand>
             <a href="#brand"><img src="../assets/img/IA Logo.png" alt="" /></a>
            </Navbar.Brand>
            <Navbar.Toggle />
          </Navbar.Header>  */}
          <span className="logo-img">
             <a href="http://www.indusaction.org/">
              <img src={logo} alt="Indus Action" height="55px" />
            </a> 
          </span>         
          <span className="dec">Campaign-Admin</span>
        
          {/* <Nav>
            <NavItem className="pk">
           pankaj  kumar
            </NavItem>
          </Nav> */}
          <Nav pullRight>
            <NavItem eventKey={1} href="/login">
              Login
            </NavItem>
            <NavItem eventKey={3}href="/signup">
              Sign up
            </NavItem>
          </Nav>
        </div>
      </div>
    );
  }
}

export default HeaderLinks;
