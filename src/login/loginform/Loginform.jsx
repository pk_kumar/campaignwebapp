import React, { Component } from "react";
import { Button, FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import {connect} from 'react-redux';
import {Redirect} from 'react-router-dom';
import { bindActionCreators } from 'redux';
import "../assets/css/Login.css";
// import AllAip from '../../services/api'
import * as Alluser from '../../redux/actions/authAction'   // this is action file authaction 
import LoginHeader  from '../Header/Header';

class Loginform extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isAuthenticated: false,
        username: "",
        password: ""  ,
        token: '',
        user: null,
        notificationSystem: null
    }  
     
    // this.login = this.login.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.validateForm = this.validateForm.bind(this);
       
  }
  
//  login() {   
//   if(this.state.username && this.state.password){
//     AllAip.login(this.state).then(response =>{       
//         // console.log(response.status)
//         // console.log(response.statusText)
//         // console.log(response)
//         if (response.status===200) {
//           // this.setState({isAuthenticated: true});
//           // console.log("redirect="+this.state.isAuthenticated);
//         } else {
//           console.log("login fail");
//           // this.handleClick.bind(this,'bl')
//         }
//         response.json().then(body => {  
//           console.log(body)
//           this.setState({token:body});
//           console.log(this.state.token)
//          })
  
//        }) 
//    }
   
   
   
//   }

  validateForm() {
    return this.state.username.length > 0 && this.state.password.length > 0;
  }

  handleChange = event => {
    this.setState({
      [event.target.id]: event.target.value
    });
  }

  handleSubmit = event => {
       event.preventDefault();
    // this.login();
    this.props.actions.Login(this.state); 
     // login function present in authaction creator actions 
    
  }
  // componentDidMount(){
  //   console.log(this.props.users)
  // }
  
  
  render() {
    if (this.props.users===200) {
      return (<Redirect to={'/Dashboard'}/>)
      }
    return (     
      <div>
        <LoginHeader/>
       
        <div className="Login " >
        
       
       
        <form onSubmit={this.handleSubmit}>
          <FormGroup controlId="username" bsSize="large">
            <ControlLabel>UserName</ControlLabel>
            <FormControl
              autoFocus
              type="text"
              onChange={this.handleChange}
            />
          </FormGroup>
          <FormGroup controlId="password" bsSize="large">
            <ControlLabel>Password</ControlLabel>
            <FormControl
              onChange={this.handleChange}
              type="password"
            />
          </FormGroup>
          <Button
            block
            bsSize="large"
            disabled={!this.validateForm()}
            type="submit"
          >
            Login
          </Button>
          
        </form>
      </div>
                  
     </div>
    );
  }
}

function mapStateToProps(state) {  
  return {
      users: state.Auth.Status
  };
}
function matchDispatchToProps(dispatch){   // this function connect to the action ie.creator authAction 
  return{
    actions:bindActionCreators(Alluser, dispatch)   // alluser is the above import authAction  and action is alternative name for access authAction function 
  } 
}
export default connect(mapStateToProps,matchDispatchToProps)(Loginform);
