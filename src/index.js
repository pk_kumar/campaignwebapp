import React from 'react';
// import 'babel-polyfill';
import ReactDOM from 'react-dom';
import {createStore, applyMiddleware,compose} from 'redux';
import 'bootstrap/dist/css/bootstrap.min.css';
// import './index.css';
import thunk from 'redux-thunk';
// import logger from 'redux-logger';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import { Provider } from 'react-redux';
import allReducers from './redux/reducers/index';
// import promise from 'redux-promise';



const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const store = createStore(
    allReducers,
    composeEnhancers( applyMiddleware(thunk,))
);

ReactDOM.render(  
    <Provider store={store}>
        <App/>
    </Provider> ,
    document.getElementById('root'));
registerServiceWorker();
